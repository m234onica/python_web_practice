from login_tutorial import create_app
from flask_bootstrap import Bootstrap

app = create_app()
if __name__ == '__main__':
  # app.config['SECRET_KEY'] = 'aaa'
  app.run(host='127.0.0.1', debug=True)
