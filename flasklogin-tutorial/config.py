import os

class Config:
  SECRET_KEY = 'aaa'
  FLASK_APP = os.environ.get('FLASK_APP')
  FLASK_ENV = os.environ.get('FLASK_ENV')
  FLASK_DEBUG = os.environ.get('FLASK_DEBUG')

  SQLALCHEMY_DATABASE_URI = 'mysql://root:rootroot@localhost/testDB'
  SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')


  SQLALCHEMY_TRACK_MODIFICATIONS = True
