from . import db
import datetime
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

class User(UserMixin, db.Model):
  __tablename__ = 'flask_db'

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(50), nullable=False, unique=False)
  email = db.Column(db.String(40), nullable=False, unique=False)
  password = db.Column(db.String(200), primary_key=False, nullable=False, unique=False)
  website = db.Column(db.String(60), index=False, nullable=True, unique=False)
  createdAt = db.Column(db.DateTime, default=datetime.datetime.now())
  updatedAt = db.Column(
      db.DateTime, default=datetime.datetime.now(), onupdate=datetime.datetime.now)

  def set_password(self, password):
    self.password = generate_password_hash(password, method="sha256")

  def check_password(self, password):
    return check_password_hash(self.password, password)

  def __repr__(self):
    return '<User {}>'.format(self.username)
    


