from wtforms import Form, StringField, PasswordField, validators,DateField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length, Optional, Required
import datetime

class SignupForm(Form):
  name = StringField('Name', validators=[DataRequired(message=('Enter a fake name or something!'))])
  email = StringField('Email', validators=[Length(min=6, message=('Please enter a valed email address.')),
                                           Email(message='Please enter a valed email address.'),
                                           DataRequired(message=('Please enter a valed email address.'))])
  password = PasswordField('Password', validators=[DataRequired(message='Please enter password'),
                                       Length(min=6, message=('Please select a stronger password')),
                                       EqualTo('confirm', message=('Passwords must match'))])
  confirm = PasswordField('Confirm Your Password',)
  website = StringField('Website', validators=[Optional()])
  submit = SubmitField('Register')

class LoginForm(Form):
  email = StringField('Email', validators=[DataRequired('Please enter a valid email address.'),
                                Email('Please enter a valid email address.')])
  password = PasswordField('Password', validators=[DataRequired('Uhh, your password tho?')])
  submit = SubmitField('Log In')
