import os
from flask import Blueprint, render_template
from flask_assets import Environment, Bundle
from flask_login import current_user
from flask import current_app as app
from .models import User
from flask_login import login_required

main_bp = Blueprint('main_bp', __name__,
                    template_folder='templates',
                    static_folder='static')
assets = Environment(app)

@main_bp.route('/', methods=['GET'])
@login_required
def dashboard():
  return render_template('dashboard.html',
                          title='Flask-login Tutorial.',
                          template='dashboard-template',
                          current_user=current_user,
                          body="You are now logged in~")